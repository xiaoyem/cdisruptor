/*
 * Copyright (c) 2015-2020 by Xiaoye Meng.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVENTPROC_INCLUDED
#define EVENTPROC_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include "seq.h"
#include "seqbar.h"
#include "ringbuf.h"

/* FIXME: exported types */
typedef struct eventproc_s *eventproc_t;

/* FIXME: exported functions */
extern eventproc_t eventproc_new(ringbuf_t *ringbuf, seqbar_t *seqbar, size_t length, void *extra);
extern void        eventproc_free(eventproc_t *epp);
extern ringbuf_t   eventproc_get_ringbuf(eventproc_t eventproc);
extern ringbuf_t  *eventproc_get_ringbufs(eventproc_t eventproc);
extern seqbar_t    eventproc_get_seqbar(eventproc_t eventproc);
extern seqbar_t   *eventproc_get_seqbars(eventproc_t eventproc);
extern seq_t       eventproc_get_seq(eventproc_t eventproc);
extern seq_t      *eventproc_get_seqs(eventproc_t eventproc);
extern void       *eventproc_get_extra(eventproc_t eventproc);
extern size_t      eventproc_get_length(eventproc_t eventproc);

#ifdef __cplusplus
}
#endif

#endif /* EVENTPROC_INCLUDED */

