/*
 * Copyright (c) 2015-2020 by Xiaoye Meng.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *                                    track to prevent wrap
 *              +----------------------------------------------------------------+
 *              |                                                                |
 *              |                                                                v
 * +----+    +====+    +=====+    +-----+    +=====+    +-----+    +=====+    +-----+
 * | P1 |--->| RB |    | SB1 |<---| EP1 |<---| SB2 |<---| EP2 |<---| SB3 |<---| EP3 |
 * +----+    +====+    +=====+    +-----+    +=====+    +-----+    +=====+    +-----+
 *      claim   ^  get    |   waitFor           |   waitFor           |   waitFor
 *              |         |                     |                     |
 *              +---------+---------------------+---------------------+
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mem.h"
#include "seq.h"
#include "waitstg.h"
#include "seqbar.h"
#include "event.h"
#include "ringbuf.h"
#include "eventproc.h"

/* FIXME */
struct vot {
	long	value;
	long	operand1, operand2;
};

/* FIXME */
static long iterations = 1000L * 1000L * 100L;
static int num_eventprocs = 3;
static int step1 = 0, step2 = 1, step3 = 2;
static long operand2_init_val = 777L;
//static long step3count;

/* FIXME */
static void *ep_thread(void *data) {
	eventproc_t eventproc = (eventproc_t)data;
	ringbuf_t ringbuf = eventproc_get_ringbuf(eventproc);
	seqbar_t  seqbar  = eventproc_get_seqbar(eventproc);
	seq_t     seq     = eventproc_get_seq(eventproc);
	int       *step   = eventproc_get_extra(eventproc);
	long next_seq = seq_get(seq) + 1L;
	long expcount = seq_get(seq) + iterations;

	while (1) {
		long avail_seq = seqbar_wait_for(seqbar, next_seq);

		while (next_seq <= avail_seq) {
			event_t *event = ringbuf_get(ringbuf, next_seq);
			struct vot *vot = event_get_value(event);

			if (*step == 0) {
				vot->value = vot->operand1 + vot->operand2;
				//fprintf(stdout, "step1res=%ld\n", vot->value);
			} else if (*step == 1) {
				vot->value += 3L;
				//fprintf(stdout, "step2res=%ld\n", vot->value);
			} else
				if ((vot->value & 4L) == 4L)
					//fprintf(stdout, "%ld\n", ++step3count);
					;
			if (next_seq == expcount) {
				seq_set(seq, avail_seq);
				goto end;
			}
			++next_seq;
		}
		seq_set(seq, avail_seq);
	}

end:
	return NULL;
}

int main(int argc, char **argv) {
	ringbuf_t ringbuf = ringbuf_new_single(1024 * 8, waitstg_new_yielding());
	seqbar_t step1seqbar = ringbuf_new_bar(ringbuf, NULL, 0);
	eventproc_t step1eventproc = eventproc_new(&ringbuf, &step1seqbar, 1, &step1);
	seq_t step1seq = eventproc_get_seq(step1eventproc);
	seqbar_t step2seqbar = ringbuf_new_bar(ringbuf, &step1seq, 1);
	eventproc_t step2eventproc = eventproc_new(&ringbuf, &step2seqbar, 1, &step2);
	seq_t step2seq = eventproc_get_seq(step2eventproc);
	seqbar_t step3seqbar = ringbuf_new_bar(ringbuf, &step2seq, 1);
	eventproc_t step3eventproc = eventproc_new(&ringbuf, &step3seqbar, 1, &step3);
	seq_t step3seq = eventproc_get_seq(step3eventproc);
	pthread_t thread[num_eventprocs];
	struct timespec start, end;
	long i, diff;

	ringbuf_add_gatingseqs(ringbuf, &step3seq, 1);
	if (pthread_create(&thread[0], NULL, ep_thread, step1eventproc) != 0) {
		fprintf(stderr, "Error initializing step1 event processor\n");
		exit(1);
	}
	if (pthread_create(&thread[1], NULL, ep_thread, step2eventproc) != 0) {
		fprintf(stderr, "Error initializing step2 event processor\n");
		exit(1);
	}
	if (pthread_create(&thread[2], NULL, ep_thread, step3eventproc) != 0) {
		fprintf(stderr, "Error initializing step3 event processor\n");
		exit(1);
	}
	clock_gettime(CLOCK_MONOTONIC, &start);
	for (i = 0; i < iterations; ++i) {
		long next = ringbuf_next(ringbuf);
		event_t *event = ringbuf_get(ringbuf, next);
		struct vot *vot = event_get_value(event);

		if (vot == NULL) {
			/* FIXME */
			NEW(vot);
			vot->operand1 = i;
			vot->operand2 = --operand2_init_val;
			event_set_value(event, vot);
		} else {
			vot->operand1 = i;
			vot->operand2 = --operand2_init_val;
		}
		ringbuf_publish(ringbuf, next);
	}
	for (i = 0; i < num_eventprocs; ++i)
		pthread_join(thread[i], NULL);
	clock_gettime(CLOCK_MONOTONIC, &end);
	diff = end.tv_sec * 1000000000 + end.tv_nsec - start.tv_sec * 1000000000 - start.tv_nsec;
	fprintf(stdout, "%ld ops/sec\n", iterations * 1000000000 / diff);
	return 0;
}

