/*
 * Copyright (c) 2015-2020 by Xiaoye Meng.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *                    track to prevent wrap
 *              +-------------------------------+
 *              |                               |
 *              |                               v
 * +----+    +====+               +=====+    +-----+
 * | P1 |--->| RB |<--------------| SB2 |<---| EP3 |
 * +----+    +====+               +=====+    +-----+
 *      claim   ^  get               |  waitFor
 *              |                    |
 *           +=====+    +-----+      |
 *           | SB1 |<---| EP1 |<-----+
 *           +=====+    +-----+      |
 *              ^                    |
 *              |       +-----+      |
 *              +-------| EP2 |<-----+
 *              waitFor +-----+
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mem.h"
#include "seq.h"
#include "waitstg.h"
#include "seqbar.h"
#include "event.h"
#include "ringbuf.h"
#include "eventproc.h"

/* FIXME */
struct vfb {
	long	value;
	int	fizz, buzz;
};

/* FIXME */
static long iterations = 1000L * 1000L * 100L;
static int num_eventprocs = 3;
static int fizz = 0, buzz = 1, fizzbuzz = 2;
//static long fizzbuzzcount;

/* FIXME */
static void *ep_thread(void *data) {
	eventproc_t eventproc = (eventproc_t)data;
	ringbuf_t ringbuf = eventproc_get_ringbuf(eventproc);
	seqbar_t  seqbar  = eventproc_get_seqbar(eventproc);
	seq_t     seq     = eventproc_get_seq(eventproc);
	int       *type   = eventproc_get_extra(eventproc);
	long next_seq = seq_get(seq) + 1L;
	long expcount = seq_get(seq) + iterations;

	while (1) {
		long avail_seq = seqbar_wait_for(seqbar, next_seq);

		while (next_seq <= avail_seq) {
			event_t *event = ringbuf_get(ringbuf, next_seq);
			struct vfb *vfb = event_get_value(event);

			if (*type == 0) {
				if (vfb->value % 3L == 0)
					vfb->fizz = 1;
			} else if (*type == 1) {
				if (vfb->value % 5L == 0)
					vfb->buzz = 1;
			} else {
				if (vfb->fizz && vfb->buzz)
					//fprintf(stdout, "%ld\n", ++fizzbuzzcount);
					;
			}
			if (next_seq == expcount) {
				seq_set(seq, avail_seq);
				goto end;
			}
			++next_seq;
		}
		seq_set(seq, avail_seq);
	}

end:
	return NULL;
}

int main(int argc, char **argv) {
	ringbuf_t ringbuf = ringbuf_new_single(1024 * 8, waitstg_new_yielding());
	seqbar_t seqbar = ringbuf_new_bar(ringbuf, NULL, 0);
	eventproc_t fizzeventproc = eventproc_new(&ringbuf, &seqbar, 1, &fizz);
	eventproc_t buzzeventproc = eventproc_new(&ringbuf, &seqbar, 1, &buzz);
	seq_t seqs[2];
	seqbar_t fizzbuzzseqbar;
	eventproc_t fizzbuzzeventproc;
	seq_t fizzbuzzseq;
	pthread_t thread[num_eventprocs];
	struct timespec start, end;
	long i, diff;

	seqs[0] = eventproc_get_seq(fizzeventproc);
	seqs[1] = eventproc_get_seq(buzzeventproc);
	fizzbuzzseqbar = ringbuf_new_bar(ringbuf, seqs, 2);
	fizzbuzzeventproc = eventproc_new(&ringbuf, &fizzbuzzseqbar, 1, &fizzbuzz);
	fizzbuzzseq = eventproc_get_seq(fizzbuzzeventproc);
	ringbuf_add_gatingseqs(ringbuf, &fizzbuzzseq, 1);
	if (pthread_create(&thread[0], NULL, ep_thread, fizzeventproc) != 0) {
		fprintf(stderr, "Error initializing fizz event processor\n");
		exit(1);
	}
	if (pthread_create(&thread[1], NULL, ep_thread, buzzeventproc) != 0) {
		fprintf(stderr, "Error initializing buzz event processor\n");
		exit(1);
	}
	if (pthread_create(&thread[2], NULL, ep_thread, fizzbuzzeventproc) != 0) {
		fprintf(stderr, "Error initializing fizzbuzz event processor\n");
		exit(1);
	}
	clock_gettime(CLOCK_MONOTONIC, &start);
	for (i = 0; i < iterations; ++i) {
		long next = ringbuf_next(ringbuf);
		event_t *event = ringbuf_get(ringbuf, next);
		struct vfb *vfb = event_get_value(event);

		if (vfb == NULL) {
			/* FIXME */
			NEW(vfb);
			vfb->value = i;
			vfb->fizz  = 0;
			vfb->buzz  = 0;
			event_set_value(event, vfb);
		} else {
			vfb->value = i;
			vfb->fizz  = 0;
			vfb->buzz  = 0;
		}
		ringbuf_publish(ringbuf, next);
	}
	for (i = 0; i < num_eventprocs; ++i)
		pthread_join(thread[i], NULL);
	clock_gettime(CLOCK_MONOTONIC, &end);
	diff = end.tv_sec * 1000000000 + end.tv_nsec - start.tv_sec * 1000000000 - start.tv_nsec;
	fprintf(stdout, "%ld ops/sec\n", iterations * 1000000000 / diff);
	return 0;
}

